export enum LogLevel {
  LOG = 0,
  DEBUG = 1,
  INFO = 2,
  WARN = 3,
  ERROR = 4,
}

export class Log {
  public data: any;
  public level: LogLevel;
  public msg: string;
  public name: string;
  public timeStamp: number = Date.now();
  public user: string;
}
