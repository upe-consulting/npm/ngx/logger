import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Log } from './log';
import { Logger } from './logger';

import 'rxjs/add/observable/interval';

@Injectable()
export class LoggerService {

  public static get Instance(): LoggerService | null {
    return LoggerService._instance;
  }

  public static PUSH_INTERVAL: number = 10000;
  public static REST_API_PATH: string;

  public static activateLogger(): void {
    Logger.ACTIVE = true;
  }

  public static deaktivedLogger(): void {
    Logger.ACTIVE = false;
  }

  public static mute(): void {
    Logger.MUTED = true;
  }

  public static unmute(): void {
    Logger.MUTED = false;
  }

  private static _instance: LoggerService | null = null;

  public get isLoggerActive(): boolean {
    return false;
  }

  public get isDecoratorActive(): boolean {
    return false;
  }

  public get isMute(): boolean {
    return false;
  }

  public readonly logQueue: Log[] = [];

  private _intervalSubscription: Subscription | null = null;

  public constructor(private _http: Http) {
    if (!LoggerService.REST_API_PATH) {
      throw new Error('RestApi path is not set');
    }
    LoggerService._instance = this;
  }

  public log(entry: Log): void {
    this.logQueue.push(entry);
  }

  public push(): void {
    const queue = this.logQueue.slice();
    this.logQueue.splice(0, this.logQueue.length - 1);
    this._http.post(LoggerService.REST_API_PATH, queue).subscribe();
  }

  public start(): void {
    this._intervalSubscription = Observable.interval(LoggerService.PUSH_INTERVAL)
      .subscribe(() => this.push());
  }

  public stop(): void {
    if (this._intervalSubscription) {
      this._intervalSubscription.unsubscribe();
      this._intervalSubscription = null;
    }
  }

}
