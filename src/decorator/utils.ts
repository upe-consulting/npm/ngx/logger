export function MERGE_OPTIONS_IN_PLACE<T>(options: T, defaultOptions: T): T {
  for (const key in defaultOptions) {
    if (!options.hasOwnProperty(key) && defaultOptions.hasOwnProperty(key)) {
      options[key] = defaultOptions[key];
    }
  }

  return options;
}

export type ConstructorType<T> = new (...args: any[]) => T;
