import { Logger } from '../logger';
import { GET_CONFIG_OBJECT, LOGGER_DECORATOR_DISABLED } from './factories';
import { MERGE_OPTIONS_IN_PLACE } from './utils';

export interface ILogAccessorOptions {
  parameters?: boolean;
  result?: boolean;
}

const DEFAULT_OPTIONS: ILogAccessorOptions = {
  parameters: true,
  result: true,
};

export function APPLY_LOGGER_TO_ACCESSOR(
  propertyKey: string,
  descriptor: PropertyDescriptor,
  options: ILogAccessorOptions = {}): PropertyDescriptor {

  MERGE_OPTIONS_IN_PLACE(options, DEFAULT_OPTIONS);

  if (descriptor === undefined) {
    throw new Error('Method descriptor is not set!');
  }

  const get = descriptor.get;
  const set = descriptor.set;

  if (get) {
    descriptor.get = function(): any {
      const result = get.apply(this);

      if (!this.logger || !(this.logger instanceof Logger)) {
        throw new Error('Logger instance is not set!');
      }

      this.logger.log(`get ${propertyKey}`, { result });

      return result;

    };
  }

  if (set) {
    descriptor.set = function(value: any) {

      if (!this.logger || !(this.logger instanceof Logger)) {
        throw new Error('Logger instance is not set!');
      }

      this.logger.log(`set ${propertyKey}`, { value });

      set.apply(this, value);
    };
  }

  return descriptor;
}

export function ExcludeAccessor(target: any, propertyKey: string, descriptor: PropertyDescriptor): void {
  GET_CONFIG_OBJECT(target).accessors.exclude.push(propertyKey);
}

export function IncludeAccessor(target: any, propertyKey: string, descriptor: PropertyDescriptor): void {
  GET_CONFIG_OBJECT(target).accessors.include.push(propertyKey);
}

export function SkipAccessorParameters(target: any, propertyKey: string, descriptor: PropertyDescriptor): void {
  GET_CONFIG_OBJECT(target).accessors.skipParameters.push(propertyKey);
}

export function SkipAccessorResult(target: any, propertyKey: string, descriptor: PropertyDescriptor): void {
  GET_CONFIG_OBJECT(target).accessors.skipResult.push(propertyKey);
}

export function LogAccessor(options?: ILogAccessorOptions) {
  return function(target: any, propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    return LOGGER_DECORATOR_DISABLED ? descriptor : APPLY_LOGGER_TO_ACCESSOR(propertyKey, descriptor, options);
  };
}
