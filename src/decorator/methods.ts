import { Logger } from '../logger';
import { GET_CONFIG_OBJECT, LOGGER_DECORATOR_DISABLED } from './factories';
import { MERGE_OPTIONS_IN_PLACE } from './utils';

export interface ILogMethodOptions {
  parameters?: boolean;
  result?: boolean;
}

const DEFAULT_OPTIONS: ILogMethodOptions = {
  parameters: true,
  result: true,
};

export function APPLY_LOGGER_TO_METHOD(
  propertyKey: string,
  descriptor: PropertyDescriptor,
  options: ILogMethodOptions = {}): PropertyDescriptor {

  MERGE_OPTIONS_IN_PLACE(options, DEFAULT_OPTIONS);

  if (descriptor === undefined) {
    throw new Error('Method descriptor is not set!');
  }

  const originalMethod = descriptor.value;

  descriptor.value = function(): any {
    const args: any = [];
    for (let i = 0; i < arguments.length; i++) {
      args[i] = arguments[i];
    }
    // note usage of originalMethod here
    const result = originalMethod.apply(this, args);

    const data: { parameters?: any[]; result?: any } = {};

    if (options.parameters) {
      data.parameters = args;
    }

    if (options.result) {
      data.result = result;
    }

    if (!this.logger || !(this.logger instanceof Logger)) {
      throw new Error('Logger instance is not set!');
    }

    this.logger.log(propertyKey, data);

    return result;
  };

  return descriptor;
}

export function ExcludeMethod(target: any, propertyKey: string, descriptor: PropertyDescriptor): void {
  GET_CONFIG_OBJECT(target).methods.exclude.push(propertyKey);
}

export function IncludeMethod(target: any, propertyKey: string, descriptor: PropertyDescriptor): void {
  GET_CONFIG_OBJECT(target).methods.include.push(propertyKey);
}

export function SkipMethodParameters(target: any, propertyKey: string, descriptor: PropertyDescriptor): void {
  GET_CONFIG_OBJECT(target).methods.skipParameters.push(propertyKey);
}

export function SkipMethodResult(target: any, propertyKey: string, descriptor: PropertyDescriptor): void {
  GET_CONFIG_OBJECT(target).methods.skipResult.push(propertyKey);
}

export function LogMathod(options?: ILogMethodOptions) {
  return function(target: any, propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    return LOGGER_DECORATOR_DISABLED ? descriptor : APPLY_LOGGER_TO_METHOD(propertyKey, descriptor, options);
  };
}
