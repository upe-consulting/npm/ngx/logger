import { ILoggable } from '../logger';
import { APPLY_LOGGER_TO_ACCESSOR, ILogAccessorOptions } from './accessors';
import { GET_CONFIG_OBJECT, LOGGER_DECORATOR_DISABLED } from './factories';
import { APPLY_LOGGER_TO_METHOD, ILogMethodOptions } from './methods';
import { ConstructorType, MERGE_OPTIONS_IN_PLACE } from './utils';

export interface ILogClassOptions {
  accessorsOptions?: ILogAccessorOptions;
  allAccessors?: boolean;
  allMethods?: boolean;
  angularComponent?: boolean;
  angularDirective?: boolean;
  methodOptions?: ILogMethodOptions;
}

const ANGULAR_METHODS: string[] = [
  'ngOnChanges',
  'ngOnInit',
  'ngDoCheck',
  'ngAfterContentInit',
  'ngAfterContentChecked',
  'ngAfterViewInit',
  'ngAfterViewChecked',
  'ngOnDestroy',
];

const DEFAULT_OPTIONS: ILogClassOptions = {
  accessorsOptions: {},
  allAccessors: false,
  allMethods: true,
  angularComponent: false,
  angularDirective: false,
  methodOptions: {},
};

export function APPLY_LOGGER_TO_DESCRIPTOR(
  propertyKey: string,
  descriptor: PropertyDescriptor,
  methodOptions: ILogMethodOptions,
  propertyOptions: ILogAccessorOptions): PropertyDescriptor {
  return descriptor.value ?
         APPLY_LOGGER_TO_METHOD(propertyKey, descriptor, methodOptions) :
         APPLY_LOGGER_TO_ACCESSOR(propertyKey, descriptor, propertyOptions);
}

export function LogClass(options: ILogClassOptions = {}): any {
  MERGE_OPTIONS_IN_PLACE(options, DEFAULT_OPTIONS);

  return function(target: any) {
    if (LOGGER_DECORATOR_DISABLED) {
      return target;
    }

    const decoratorClassConfig = GET_CONFIG_OBJECT(target);

    const propertyKeys = Object.getOwnPropertyNames(target.prototype);

    let methods: string[] = [];
    let accessors: string[] = [];

    for (const propertyKey of propertyKeys) {
      const descriptor: PropertyDescriptor | undefined = Object.getOwnPropertyDescriptor(target.prototype, propertyKey);
      if (descriptor === undefined) {
        throw new Error(`PropertyDescriptor for: '${propertyKey}' not found!`);
      }
      if (descriptor.value) {
        methods.push(propertyKey);
      } else {
        accessors.push(propertyKey);
      }
    }

    if (options.allMethods && decoratorClassConfig.methods.exclude.length) {
      methods = methods
        .filter((propertyKey: string) => decoratorClassConfig.methods.exclude.indexOf(propertyKey) === -1);
    } else {
      methods = decoratorClassConfig.methods.include;
    }

    if (options.allAccessors && decoratorClassConfig.accessors.exclude.length) {
      accessors = accessors
        .filter((propertyKey: string) => decoratorClassConfig.accessors.exclude.indexOf(propertyKey) === -1);
    } else {
      accessors = decoratorClassConfig.accessors.include;
    }

    for (const propertyKey of methods) {
      const descriptor: PropertyDescriptor | undefined = Object.getOwnPropertyDescriptor(target.prototype, propertyKey);
      if (descriptor === undefined) {
        throw new Error(`PropertyDescriptor for: '${propertyKey}' not found!`);
      }
      APPLY_LOGGER_TO_METHOD(propertyKey, descriptor, options.methodOptions);
    }

    for (const propertyKey of accessors) {
      const descriptor: PropertyDescriptor | undefined = Object.getOwnPropertyDescriptor(target.prototype, propertyKey);
      if (descriptor === undefined) {
        throw new Error(`PropertyDescriptor for: '${propertyKey}' not found!`);
      }
      APPLY_LOGGER_TO_METHOD(propertyKey, descriptor, options.accessorsOptions);
    }

    return target;
  };
}
