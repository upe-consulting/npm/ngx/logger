export const LOGGER_KEY = '__UPE_NGX_LOGGER__';

export interface IDecoratorConfig {
  accessors: { exclude: string[]; include: string[]; skipParameters: string[]; skipResult: string[] };
  methods: { exclude: string[]; include: string[]; skipParameters: string[]; skipResult: string[] };
}

export let LOGGER_DECORATOR_DISABLED: boolean = false;

const DEFAULT_DECORATOR_CONFIG: IDecoratorConfig = {
  accessors: {
    exclude: [],
    include: [],
    skipParameters: [],
    skipResult: [],
  },
  methods: {
    exclude: ['constructor'],
    include: [],
    skipParameters: [],
    skipResult: [],
  },
};

export function GET_CONFIG_OBJECT(target: any): IDecoratorConfig {
  if (!target.hasOwnProperty(LOGGER_KEY)) {
    target[LOGGER_KEY] = JSON.parse(JSON.stringify(DEFAULT_DECORATOR_CONFIG));
  }

  return target[LOGGER_KEY];
}
