import { Log, LogLevel } from './log';
import { LoggerService } from './logger.service';

export interface ILoggable {
  readonly logger: Logger;
}

export class Logger {

  public static ACTIVE: boolean = true;
  public static DECORATOR: boolean = true;
  public static MUTED: boolean = false;

  public name: string;

  public debug(msg: string, ...data: any[]): void {
    this._log(msg, LogLevel.DEBUG, ...data);
  }

  public error(msg: string, ...data: any[]): void {
    this._log(msg, LogLevel.ERROR, ...data);
  }

  public info(msg: string, ...data: any[]): void {
    this._log(msg, LogLevel.INFO, ...data);
  }

  public log(msg: string, ...data: any[]): void {
    this._log(msg, LogLevel.LOG, ...data);
  }

  public warn(msg: string, ...data: any[]): void {
    this._log(msg, LogLevel.WARN, ...data);
  }

  private _log(msg: string, level: LogLevel, ...data: any[]): void {
    if (Logger.ACTIVE === false) {
      return;
    }

    const log = new Log();
    log.name = this.name;
    log.data = data;
    log.level = level;
    log.msg = msg;

    if (LoggerService.Instance) {
      LoggerService.Instance.log(log);
    }

    if (Logger.MUTED === false) {
      console.log(log);
    }

  }

}
