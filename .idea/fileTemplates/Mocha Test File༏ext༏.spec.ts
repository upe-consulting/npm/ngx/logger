/* tslint:disable:no-unused-variable */
/* tslint:disable:no-unused-expression */
import {expect} from 'chai';
import {suite, test} from 'mocha-typescript';

#set($FileName = ${StringUtils.removeAndHump(${NAME})})
#set($FileName = $FileName.substring(0,1).toUpperCase() + $FileName.substring(1) + "Test")
@suite class $FileName {

  #[[$END$]]#

}
